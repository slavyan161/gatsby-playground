import React from "react";
import { ReactComponentElement } from "react";

declare interface ICard {
    title: string;
    description: string;
    imgUrl?: string;
    children?: ReactComponentElement<any>;
}

export const Card = (
    {
        title, 
        description, 
        imgUrl,
        children
    }: ICard
) => {

    return (
        <div className="border border-gray-600">
            
            {children}
        </div>
    )
}